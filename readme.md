# Image

To be able to store image add `HandleImages` trait, and define properties which will have Image as a its value:


    class User 
    {
        use HandleImages;

        protected $casts = [
            'profile_photo' => 'image'
        ];


        public function getProfilePhotoAttribute($value)
        {
            return $this->getImageValue($value);
        }

        public function setProfilePhotoAttribute($image)
        {
            $this->setImageValue('profile_photo', $image);
        }

        public function getImagesFolder()
        {
            // define folder name where images will be stored
            return 'user-' . snake_case($this->name);
        }

        public function getImageSizes()
        {
            // Additional configuration for this model
            return [
                'cover' => [
                    'width' => 900,
                    'height' => 300
                ]
            ];
        }
    }

Returned image will be type of `Image`.

    $user->image this will be image.


### Setting image

To set image:

    $user->image = Image::create('image_url');

Or multiple sizes image:

    $image = Image::create('url');
    // add thumb size
    $image->add('thumb', resizeImageGetPath($image));

    $user->image = $image;


To store files you can use `ImageStore`:

    public function files(City $city, Request $request, ImageStore $imageStore)
    {
        $this->validate($request, [
          'image' => 'image|max:4000',
        ]);
      
        $imageStore->handle($city, $request->allFiles());
        $city->save();

        return $city;
    }

You can pull every size like they are properties.

    $product->image->main // for main image size
    $product->image->url() // original url
    $product->image // it will return default url 

### Image configuration


Global settings.

    Image::$defaultConfiguration = [
      'default_size' => 'url' // Size that will be return by ->__toString() method
      'default_lang' => 'en' // Default language when setting alt text
    ];

or while creating image.

    Image::create($data, $config);

Only properties with cast => image will be handled by ImageStore

# Gallery

To use this with model add casts.

    class Property extends Model {

        use HandleImages;

        protected $casts = [
            'slider' => 'gallery',
        ];

        public function getSliderAttribute($value)
        {
            return $this->getGalleryValue($value);
        }

        public function setSliderAttribute($gallery)
        {
            $this->setGalleryValue('slider', $gallery);
        }
    }
    
Add to migration 

    $table->longText('slider')->nullable();
    
Add to api.php
    
    Route::post('properties/{property?}/files', 'PropertiesController@files');

Do not add `slider` to `$fillable`.

    public function files(Group $group, Request $request, ImageStore $imageStore)
    {
        $this->validate($request, [
          'slider.*' => 'image|max:4000',
        ]);

        $imageStore->handle($group, $request->allFiles());
        $group->save();

        return $group;
    }

Instead use `storeGallery` method which will delete removed image on frontend.

    public function save(Group $group, Request $request, ImageStore $imageStore)
    {
        $group->fill($request->input());
        $imageStore->storeGallery($group, $request->input());
        $group->save();
    }

TODO: On delete method

In admin use `GalleryUpload.vue`

    <gallery-upload v-model="form.slider" @change="files.slider = $event"></gallery-upload>

    <script>
        import UploadFiles from 'lib/UploadFiles';
        export default {
                mixins: [UploadFiles],
                data() {
                    return {
                        form: new Form(() => {
                            return {
                                slider:[],
                            }
                        })
                    }
                },
             }
    </script>

## On frontend in blade

    @foreach($product->slider as $image)
        <img src="{{ $image }}" />
    @endforeach
