<?php

namespace dam1r89\Image;

trait HandleImages
{
    public function getImageValue($value)
    {
        return Image::create(json_decode($value, true) ?: []);
    }

    public function setImageValue($key, $value)
    {
        $this->attributes[$key] = json_encode($value);
    }

    public function getGalleryValue($value)
    {
        return Gallery::create(json_decode($value, true) ?: []);
    }

    public function setGalleryValue($key, $value)
    {
        $this->attributes[$key] = json_encode($value);
    }

    public function getMissingImages($key, $new)
    {
        $old = $this->$key;
        if (!$old instanceof Gallery) {
            throw new \Exception('You must set getter/setter for gallery type of fields');
        }
        return $new->missing($old);
    }

    public function getImagesFolder()
    {
        return snake_case(class_basename($this));
    }

    public function getImageSizes()
    {
        return [];
    }
}
