<?php

namespace dam1r89\Image;

use ArrayAccess;
use Countable;
use JsonSerializable;
use IteratorAggregate;
use ArrayIterator;

class Gallery implements JsonSerializable, ArrayAccess, Countable, IteratorAggregate
{
    private $images;

    public static function create(array $images = [])
    {
        return new self($images);
    }

    private function __construct(array $data)
    {
        $this->parse($data);
    }

    private function parse($data)
    {
        $this->images = [];
        foreach ($data as $image) {
            if (is_array($image)) {
                $this->images[] = Image::create($image);
            } elseif ($image instanceof Image) {
                $this->images[] = $image;
            } else {
                throw new \Exception('Element should be instance of Image or array.');
            }
        }
    }

    public function add($image)
    {
        $this->images[] = $image;
    }

    public function toArray()
    {
        $serialized = [];
        foreach ($this->images as $image) {
            $serialized[] = $image->toArray();
        }

        return $serialized;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function missing(Gallery $gallery, $bySize = null)
    {
        $images = [];
        foreach ($this as $image) {
            $size = $image->$bySize ?: $image->url();
            $images[$size] = $image;
        }

        foreach ($gallery as $image) {
            $key = $image->$bySize ?: $image->url();
            unset($images[$key]);
        }

        return array_values($images);
    }

    public function remove($images)
    {
        foreach((array)$images as $image) {
            foreach($this->images as $key => $currentImage) {
                if ($currentImage->url() === $image->url()) {
                    unset($this->images[$key]);
                }
            }
        }

        $this->images = array_values($this->images);

        return $this;
    }


    public function union($images)
    {
        foreach($images as $image) {
            $this->add($image);
        }
        return $this;
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->images[] = $value;
        } else {
            $this->images[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->images[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->images[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->images[$offset]) ? $this->images[$offset] : null;
    }

    public function count()
    {
        return count($this->images);
    }

    public function getIterator()
    {
        return new ArrayIterator($this->images);
    }
}
