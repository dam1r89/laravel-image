<?php

namespace dam1r89\Image;

use JsonSerializable;

class Image implements JsonSerializable
{
    protected $data;
    protected $config;

    public static $defaultConfig = [
        'default_size' => 'url',
        'default_lang' => 'en',
    ];

    public static function create($data, array $config = [])
    {
        if (!is_array($data)) {
            $data = ['url' => $data];
        }

        return new static($data, $config);
    }

    private function __construct(array $data, array $config)
    {
        // make sure that alt exists and it is object
        $data = array_merge(['alt' => []], $data);
        $data['alt'] = $data['alt'];
        $this->data = $data;
        $this->config = array_merge(self::$defaultConfig, $config);
    }

    public function url($url = null)
    {
        if (null === $url) {
            return $this->get('url');
        }
        $this->data['url'] = $url;
        return $this;
    }

    public function add($size, $url)
    {
        $this->data['sizes'][$size] = $url;
        return $this;
    }

    public function alt($data = null)
    {
        // set alt text
        if (is_array($data)) {
            $this->setAlt($data);

            return $this;
        }

        // Get default language
        if (is_null($data)) {
            return $this->get('alt.'.$this->config['default_lang']);
        }
        // Get specific language
        return $this->get('alt.'.$data);
    }

    private function setAlt(array $data)
    {
        $this->data['alt'] = $data;
    }

    public function __toString()
    {
        return $this->get('sizes.'.$this->config['default_size'], $this->get('url')) ?: '';
    }

    public function __get($name)
    {
        return $this->get("sizes.$name");
    }

    protected function get($key, $default = null)
    {
        $array = (array) $this->data;
        foreach (explode('.', $key) as $segment) {
            if (isset($array[$segment])) {
                $array = $array[$segment];
            } else {
                return $default;
            }
        }

        return $array;
    }

    public function toArray()
    {
        return $this->data;
    }

    public function jsonSerialize()
    {
        $data = $this->toArray();
        $data['alt'] = (object) $data['alt'];

        return $data;
    }

    public function sizes()
    {
        return array_keys($this->get('sizes', []));
    }
}
