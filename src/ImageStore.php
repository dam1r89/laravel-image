<?php

namespace dam1r89\Image;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManager;
use Storage;

class ImageStore
{
    public $config = [
        'sizes' => [
            'main' => [
                'width' => 1024,
            ],
            'thumb' => [
                'width' => 300,
            ],
        ],
    ];
    private $images;
    private $disk;

    public function __construct(ImageManager $images, array $config = [])
    {
        $this->config = array_merge($this->config, $config);
        $this->images = $images;
        $this->disk = Storage::disk('public');
    }

    public function handle(Model $model, array $files)
    {

        foreach ($files as $name => $file) {
            if ($model->hasCast($name, 'image')) {
                $this->handleImage($model, $file, $name);
            }
            if ($model->hasCast($name, 'gallery')) {
                $this->handleGallery($model, $file, $name);
            }
        }

        return $model;
    }

    private function handleImage($model, $file, $key)
    {
        $oldImage = $model->$key;

        $model->$key = $this->makeSizes($model, $file);

        // Delete old images
        $this->deleteImage($model, $oldImage);
    }

    public function deleteImage($model, $image)
    {
        if (is_null($image)) {
            return;
        }

        $folder = $model->getImagesFolder();

        if (preg_match('/'.$folder.'\/(.*)$/', $image, $matches)) {
            $this->disk->delete($folder.'/'.$matches[1]);
        }

        // Delete sizes
        foreach ($image->sizes() as $sizeName) {
            if (preg_match('/'.$folder.'\/'.$sizeName.'\/(.*)$/', $image->$sizeName, $matches)) {
                $this->disk->delete($folder.'\/'.$sizeName.'/'.$matches[1]);
            }
        }
    }

    private function handleGallery($model, $files, $key)
    {

        /**
         * @var Gallery
         */
        $gallery = $model->$key;

        foreach ($files as $index => $file) {
            $gallery->add(
                $this->makeSizes($model, $file)
            );
        }

        $model->$key = $gallery;
    }

    /**
     * Used for removing or attaching images from gallery
     * without handling uploads.
     *
     * @param $model
     * @param $request
     * @param $folder
     *
     * @return mixed
     */
    public function storeGallery(Model $model, $request)
    {
        $folder = $model->getImagesFolder();

        foreach ($request as $name => $gallery) {
            if ($model->hasCast($name, 'gallery')) {
                $newGallery = Gallery::create($gallery);
                $missing = $model->getMissingImages($name, $newGallery);
                foreach ($missing as $image) {
                    $this->deleteImage($model, $image);
                }
                $model->$name = $newGallery;
            }
        }

        return $model;
    }

    public function deleteImages($model)
    {
        foreach ($model->getCasts() as $name => $type) {
            if ('image' === $type) {
                $this->deleteImage($model, $model->$name);
            }
            if ('gallery' === $type) {
                foreach ($model->$name as $image) {
                    $this->deleteImage($model, $image);
                }
            }
        }

        return $model;
    }

    /**
     * @param $image
     * @param $file
     *
     * @return Image
     */
    public function makeSizes(Model $model, $file)
    {
        $folder = $model->getImagesFolder();

        $path = $file->hashName($folder);

        // TODO: Maybe just to copy image without going through intervention?
        $this->disk->put(
            $path, $this->images->make($file->path())->encode()
        );

        $image = Image::create($this->disk->url($path));

        $sizes = array_merge($this->config['sizes'], $model->getImageSizes());

        foreach ($sizes as $sizeName => $config) {
            $path = $file->hashName($folder.'\/'.$sizeName);
            $this->disk->put(
                $path, $this->resize($file->path(), $config)
            );
            $image->add($sizeName, $this->disk->url($path));
        }

        return $image;
    }

    protected function resize($path, $config)
    {
        $config = array_merge([
            'width' => null,
            'height' => null,
            'upsize' => false,
            'format' => null,
            'quality' => null,
        ], $config);

        return $this->images->make($path)
            ->fit(
                $config['width'],
                $config['height'],
                function ($constraint) use ($config) {
                    if (!$config['upsize']) {
                        $constraint->upsize();
                    }
                }
            )->encode($config['format'], $config['quality']);
    }
}
