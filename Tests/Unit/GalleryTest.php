<?php

namespace App\Images\Tests\Feature;

use App\Images\Gallery;
use App\Images\Image;
use Tests\TestCase;

class GalleryTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function testExample()
    {
        $gallery = Gallery::create();
        $gallery->add(Image::create(['url' => 'http://domain.com/image.jpg']));
        $gallery->add(Image::create(['url' => 'http://domain.com/image2.jpg']));

        $data = [
            [
                'url' => 'http://domain.com/image.jpg',
                'alt' => [],
            ], [
                'url' => 'http://domain.com/image2.jpg',
                'alt' => [],
            ],
        ];

        $this->assertEquals($data, $gallery->toArray());

        $this->assertEquals(Gallery::create($data)->toArray(), $gallery->toArray());
    }

    public function testCreatingWithImageArray()
    {
        $images = [
            Image::create(['url' => 'http://domain.com/image.jpg']),
            Image::create(['url' => 'http://domain.com/image2.jpg']),
        ];

        $gallery = Gallery::create($images);

        $data = [
            [
                'url' => 'http://domain.com/image.jpg',
                'alt' => [],
            ], [
                'url' => 'http://domain.com/image2.jpg',
                'alt' => [],
            ],
        ];

        $this->assertEquals($data, $gallery->toArray());
    }

    public function testComplexImages()
    {
        $image = Image::create('http:\\domain.com\original.jpg', ['default_lang' => 'en']);
        $image->add('main', 'http://path.net/main.jpg');
        $image->add('thumb', 'http://path.net/thumb.jpg');
        $image->alt(['en' => 'photo', 'sr' => 'slika']);
        $data = [[
            'url' => 'http:\\domain.com\original.jpg',
            'alt' => [
                'en' => 'photo',
                'sr' => 'slika',
            ],
            'sizes' => [
                'main' => 'http://path.net/main.jpg',
                'thumb' => 'http://path.net/thumb.jpg',
            ],
        ]];
        $gallery = Gallery::create($data);
        $this->assertEquals($data, $gallery->toArray());
        $this->assertEquals(Gallery::create($data)->toArray(), $gallery->toArray());
    }

    public function testDetectingDiff()
    {

        $old = Gallery::create([
            Image::create('http://domain.com/image.jpg'),
            Image::create('http://domain.com/image2.jpg'),
        ]);

        $new = Gallery::create([
            Image::create('http://domain.com/image2.jpg'),
        ]);

        $missing = $old->missing($new);

        $this->assertEquals(1, count($missing));
        $this->assertEquals('http://domain.com/image.jpg', (string)$missing[0]);

    }

    public function testDetectingMissingByKey()
    {
        $old = Gallery::create([
            Image::create('image1-url')->add('source', 'image1-source'),
            Image::create('image2-url')->add('source', 'image2-source'),
        ]);

        $new = Gallery::create([
            Image::create('image1-url')->add('source', 'image-source-changed'),
            Image::create('image2-url')->add('source', 'image2-source'),
        ]);

        $missing = $new->missing($old);
        $this->assertEquals(0, count($missing));

        $missing = $new->missing($old, 'source');
        $this->assertEquals(1, count($missing));
        $this->assertEquals('image1-url', (string)$missing[0]);
    }

    public function testRemovingImages()
    {
        $gallery = Gallery::create([
            Image::create('image1-url'),
            Image::create('image2-url'),
        ])->remove([
            Image::create('image1-url')
        ]);
        $this->assertEquals(1, count($gallery));
        $this->assertEquals('image2-url', $gallery[0]);


    }

    public function testGalleryUnion()
    {
        $gallery = Gallery::create([
            Image::create('image1-url'),
            Image::create('image2-url'),
        ])->union([
            Image::create('image3-url')
        ]);

        $this->assertEquals(3, count($gallery));
        $this->assertEquals('image1-url', $gallery[0]);
        $this->assertEquals('image3-url', $gallery[2]);
    }

    public function testAccessingAsArray()
    {
        $images = [
            Image::create(['url' => 'http://domain.com/image.jpg']),
            Image::create(['url' => 'http://domain.com/image2.jpg']),
        ];

        $gallery = Gallery::create($images);
        $this->assertNotNull($gallery[0]);
        $this->assertTrue($gallery[0] instanceof Image);
    }

    public function testIterator()
    {
        $images = [
            Image::create(['url' => 'http://domain.com/image.jpg']),
            Image::create(['url' => 'http://domain.com/image2.jpg']),
        ];

        $gallery = Gallery::create($images);
        $count = 0;
        foreach ($gallery as $image) {
            $count += 1;
        }

        $this->assertEquals(2, $count);
    }
}
