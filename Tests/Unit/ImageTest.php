<?php

namespace App\Images\Tests\Feature;

use App\Images\Image;
use Tests\TestCase;

class ImageTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function testRecreatingImageFromData()
    {
        $data = ['url' => 'http://path.net/image.jpg', 'sizes' => [
            'main' => 'http://path.net/main.jpg',
            'thumb' => 'http://path.net/thumb.jpg',
        ]];

        $image = Image::create($data);

        $this->assertEquals('http://path.net/image.jpg', $image->__toString());
        $this->assertEquals('http://path.net/main.jpg', $image->main);
        $this->assertEquals('http://path.net/thumb.jpg', $image->thumb);
    }

    public function testCreatingInitialImage()
    {
        $image = Image::create('http:\\domain.com\original.jpg');

        $image->add('main', 'http://path.net/main.jpg');
        $this->assertEquals('http://path.net/main.jpg', $image->main);
    }

    public function testConfiguration()
    {
        Image::$defaultConfig = ['default_size' => 'main'];
        $image = Image::create('http:\\domain.com\original.jpg');

        $image->add('main', 'http://path.net/main.jpg');
        $this->assertEquals('http://path.net/main.jpg', $image->__toString());

        Image::$defaultConfig = ['default_size' => 'notexisting'];
        $image = Image::create('http:\\domain.com\original.jpg');
        $this->assertEquals('http:\\domain.com\original.jpg', $image->__toString());
    }

    public function testSettingTags()
    {
        $image = Image::create('http:\\domain.com\original.jpg', ['default_lang' => 'en']);
        $image->alt(['en' => 'Belgrade', 'sr' => 'Beograd']);
        $this->assertEquals('Belgrade', $image->alt());
        $this->assertEquals('Belgrade', $image->alt('en'));
        $this->assertEquals('Beograd', $image->alt('sr'));
    }

    public function testSerializeAndDeserializeImage()
    {
        $image = Image::create('http:\\domain.com\original.jpg', ['default_lang' => 'en']);
        $image->add('main', 'http://path.net/main.jpg');
        $image->add('thumb', 'http://path.net/thumb.jpg');
        $image->alt(['en' => 'photo', 'sr' => 'slika']);
        $data = [
            'url' => 'http:\\domain.com\original.jpg',
            'alt' => [
                'en' => 'photo',
                'sr' => 'slika',
            ],
            'sizes' => [
                'main' => 'http://path.net/main.jpg',
                'thumb' => 'http://path.net/thumb.jpg',
            ],
        ];

        $this->assertEquals($data, $image->toArray());
        $this->assertEquals(Image::create($data)->toArray(), $image->toArray());
        $this->assertEquals(json_encode($image->toArray()), json_encode($image));
    }

    public function testGettingSizes()
    {
        $image = Image::create('http:\\domain.com\original.jpg', ['default_lang' => 'en']);
        $image->add('main', 'http://path.net/main.jpg');
        $image->add('thumb', 'http://path.net/thumb.jpg');
        $this->assertEquals(['main', 'thumb'], $image->sizes());
    }

    public function testNotSettingUrl()
    {
        $image = Image::create(['alt' => 'something']);
        $this->assertEquals('', (string) $image);
    }

    public function testSettingUrlLater()
    {
        $image = Image::create(['alt' => 'something']);
        $this->assertEquals('', (string) $image);
        $image->url('http://domain.com/photo.jpg');
        $this->assertEquals('http://domain.com/photo.jpg', (string) $image);
    }
}
