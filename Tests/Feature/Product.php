<?php

namespace App\Images\Tests\Feature;

use App\Images\HandleImages;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HandleImages;

    protected $casts = [
        'photo' => 'image',
        'slider' => 'gallery',
    ];

    public function getPhotoAttribute($value)
    {
        return $this->getImageValue($value);
    }

    public function setPhotoAttribute($image)
    {
        $this->setImageValue('photo', $image);
    }

    public function getSliderAttribute($value)
    {
        return $this->getGalleryValue($value);
    }

    public function setSliderAttribute($gallery)
    {
        $this->setGalleryValue('slider', $gallery);
    }

    public function getImagesFolder()
    {
        return 'test-product-folder';
    }
}
