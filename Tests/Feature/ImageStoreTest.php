<?php

namespace App\Images\Tests\Feature;

use App\Images\Gallery;
use App\Images\Image;
use App\Images\ImageStore;
use Illuminate\Http\File;
use Storage;
use Tests\TestCase;

class ImageStoreTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function testStoringAndRemovingImages()
    {
        $product = new Product();
        $imageStore = app(ImageStore::class);
        $files = ['photo' => new File(__DIR__.'/image.jpg', 'image.jpg')];
        $imageStore->handle($product, $files);
        $this->assertTrue($product->photo instanceof Image);

        $disk = Storage::disk('public');

        preg_match('/test-product-folder\/(.*)$/', $product->photo, $matches);
        $this->assertTrue($disk->has('test-product-folder/'.$matches[1]));
        $this->assertTrue($disk->has('test-product-folder-main/'.$matches[1]));
        $this->assertTrue($disk->has('test-product-folder-thumb/'.$matches[1]));

        $imageStore->deleteImages($product);
        $this->assertFalse($disk->has('test-product-folder/'.$matches[1]));
        $this->assertFalse($disk->has('test-product-folder-main/'.$matches[1]));
        $this->assertFalse($disk->has('test-product-folder-thumb/'.$matches[1]));
    }

    /**
     * A basic test example.
     */
    public function testChangingImageShouldDeleteOldVersion()
    {
        $product = new Product();
        $imageStore = app(ImageStore::class);

        $files = ['photo' => new File(__DIR__.'/image.jpg', 'image.jpg')];
        $imageStore->handle($product, $files);
        $this->assertTrue($product->photo instanceof Image);

        $disk = Storage::disk('public');

        preg_match('/test-product-folder\/(.*)$/', $product->photo, $matches);
        $this->assertTrue($disk->has('test-product-folder/'.$matches[1]));
        $this->assertTrue($disk->has('test-product-folder-main/'.$matches[1]));
        $this->assertTrue($disk->has('test-product-folder-thumb/'.$matches[1]));

        $files = ['photo' => new File(__DIR__.'/image2.jpg', 'image2.jpg')];
        $imageStore->handle($product, $files);

        $this->assertFalse($disk->has('test-product-folder/'.$matches[1]));
        $this->assertFalse($disk->has('test-product-folder-main/'.$matches[1]));
        $this->assertFalse($disk->has('test-product-folder-thumb/'.$matches[1]));

        $imageStore->deleteImages($product);
    }

    public function testAppendingImageAfterAlt()
    {
        $product = new Product();
        $this->assertTrue($product->photo instanceof Image);
        $this->assertNotNull($product->photo);

        $imageStore = app(ImageStore::class);
        $files = ['photo' => new File(__DIR__.'/image.jpg', 'image.jpg')];
        $imageStore->handle($product, $files);

        $this->assertTrue('' != (string) $product->photo);

        $imageStore->deleteImages($product);
    }

    public function _testNullValue()
    {
        $product = new Product();
        $serialized = $product->toArray();
        $this->assertTrue(isset($serialized['photo']));
        $serializedImage = $serialized['photo'];
        $this->assertNotNull($serializedImage);
    }

    public function testUploadingGallery()
    {
        $product = new Product();
        $imageStore = app(ImageStore::class);
        $files = ['slider' => [
            new File(__DIR__.'/image.jpg', 'image.jpg'),
            new File(__DIR__.'/image2.jpg', 'image2.jpg'),
        ]];

        $imageStore->handle($product, $files);
        $this->assertTrue($product->slider instanceof Gallery);

        $disk = Storage::disk('public');

        $this->assertEquals(2, count($product->slider));
        $this->assertTrue($product->slider[0] instanceof Image);

        preg_match('/test-product-folder\/(.*)$/', $product->slider[0], $matches);
        $file1 = $matches[1];
        $this->assertTrue($disk->has('test-product-folder/'.$file1));
        $this->assertTrue($disk->has('test-product-folder-main/'.$file1));
        $this->assertTrue($disk->has('test-product-folder-thumb/'.$file1));

        preg_match('/test-product-folder\/(.*)$/', $product->slider[1], $matches);
        $file2 = $matches[1];
        $this->assertTrue($disk->has('test-product-folder/'.$file2));
        $this->assertTrue($disk->has('test-product-folder-main/'.$file2));
        $this->assertTrue($disk->has('test-product-folder-thumb/'.$file2));

        $imageStore->deleteImages($product);
        $this->assertFalse($disk->has('test-product-folder/'.$file1));
        $this->assertFalse($disk->has('test-product-folder-main/'.$file1));
        $this->assertFalse($disk->has('test-product-folder-thumb/'.$file1));
        $this->assertFalse($disk->has('test-product-folder/'.$file2));
        $this->assertFalse($disk->has('test-product-folder-main/'.$file2));
        $this->assertFalse($disk->has('test-product-folder-thumb/'.$file2));
    }

    public function testGalleryDiff()
    {
        $product = new Product();
        $product->slider = Gallery::create([
            Image::create('http://domain.com/image1.jpg'),
            Image::create('http://domain.com/image2.jpg'),
        ]);

        $request = $product->slider->toArray();
        unset($request[0]);
        $missing = $product->getMissingImages('slider', Gallery::create($request));

        $this->assertEquals(1, count($missing));
        $this->assertEquals($product->slider[0]->url(), $missing[0]->url());
    }

    public function testSettingChangedGallery()
    {
        $product = new Product();
        $imageStore = app(ImageStore::class);
        $files = ['slider' => [
            new File(__DIR__.'/image.jpg', 'image.jpg'),
            new File(__DIR__.'/image2.jpg', 'image2.jpg'),
        ]];

        $imageStore->handle($product, $files);
        $this->assertTrue($product->slider instanceof Gallery);

        $disk = Storage::disk('public');

        $this->assertEquals(2, count($product->slider));
        $this->assertTrue($product->slider[0] instanceof Image);

        preg_match('/test-product-folder\/(.*)$/', $product->slider[0], $matches);
        $file1 = $matches[1];
        $this->assertTrue($disk->has('test-product-folder/'.$file1));

        preg_match('/test-product-folder\/(.*)$/', $product->slider[1], $matches);
        $file2 = $matches[1];
        $this->assertTrue($disk->has('test-product-folder/'.$file2));

        $slider = $product->slider->toArray();
        unset($slider[1]);

        $imageStore->storeGallery($product, ['slider' => $slider]);

        $this->assertEquals(1, count($product->slider));

        $this->assertTrue($disk->has('test-product-folder/'.$file1));
        $this->assertFalse($disk->has('test-product-folder/'.$file2));

        $imageStore->deleteImages($product);
    }
}
